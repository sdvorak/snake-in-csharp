﻿using System;
using System.Collections.Generic;

namespace SnakeZapoctovyProgramProgII
{
    public enum Direction { TOP, BOTTOM, LEFT, RIGHT };

    public class Snake
    {
        public readonly int size = 10;
        // snake with its parts
        private LinkedList<(int, int)> snake;
        private Direction headDirection = Direction.TOP;
        private HashSet<(int, int)> snakeParts;

        public Snake()
        {
            snake = new LinkedList<(int, int)>();
            snake.AddFirst((200, 200));
        }

        public void AddPart()
        {
            this.snake.AddLast(this.GetTail());
        }

        public bool IsColliding((int, int) part1, (int, int) part2)
        {
            if (part1.Item1 == part2.Item1 && part1.Item2 == part2.Item2) return true;

            return false;
        }

        public bool IsSnakeEatingItself()
        {
            snakeParts = new HashSet<(int, int)>();
            foreach ((int, int) part in snake)
            {
                if (snakeParts.Contains(part)) return true;
                else snakeParts.Add(part);
            }

            return false;
        }

        public void ChangeHeadDirection(Direction newDirection)
        {
            this.headDirection = newDirection;
        }

        public (int, int) GetTail()
        {
            return snake.Last.Value;
        }

        public (int, int) GetHead()
        {
            return snake.First.Value;
        }

        public (int, int) GetNextCoordOfHead()
        {
            (int, int) head = GetHead();
            switch (headDirection)
            {
                case Direction.TOP:
                    head.Item2 = head.Item2 - size;
                    break;
                case Direction.BOTTOM:
                    head.Item2 = head.Item2 + size;
                    break;
                case Direction.LEFT:
                    head.Item1 = head.Item1 - size;
                    break;
                case Direction.RIGHT:
                    head.Item1 = head.Item1 + size;
                    break;
            }

            return head;
        }

        public void Next()
        {
            // remove last
            // add first incremented in dir
            snake.AddFirst(GetNextCoordOfHead());
            snake.RemoveLast();
        }

        public LinkedList<(int, int)> GetSnake()
        {
            return snake;
        }
    }
}