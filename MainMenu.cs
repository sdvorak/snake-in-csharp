﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace SnakeZapoctovyProgramProgII
{
    public class MainMenu : Form
    {
        public TableLayoutPanel root;

        public Button buttonNewGame;
        public Button buttonExitGame;
        public Label scoreLabel;

        public MainMenu()
        {
            InitComponent();
        }

        private void InitComponent()
        {
            this.Text = "Snake Main Menu";
            this.ClientSize = new Size(550, 450);

            buttonNewGame = new Button();
            buttonNewGame.Text = "New Game";
            buttonNewGame.Size = new Size(200, 100);
            buttonNewGame.Font = new Font("Arial", 25, FontStyle.Bold);
            buttonNewGame.Click += new EventHandler(ButtonNewGameEventHandler);

            buttonExitGame = new Button();
            buttonExitGame.Text = "Exit Game";
            buttonExitGame.Size = new Size(200, 100);
            buttonExitGame.Font = new Font("Arial", 25, FontStyle.Bold);
            buttonExitGame.Click += new EventHandler(ButtonExitGameEventHandler);

            scoreLabel = new Label();
            scoreLabel.Font = new Font("Arial", 40, FontStyle.Bold);
            scoreLabel.Size = new Size(Width, 70);
            scoreLabel.Text = "Score: " + Game.score;
            scoreLabel.TextAlign = ContentAlignment.TopCenter;

            root = new TableLayoutPanel();
            root.RowCount = 1;
            root.ColumnCount = 2;
            root.AutoSize = true;


            // display score
            if (!Game.firstGame)
            {
                this.Controls.Add(scoreLabel);
            }
            root.Controls.Add(buttonNewGame, 0, 0);
            root.Controls.Add(buttonExitGame, 1, 0);

            this.Controls.Add(root);
            this.CenterToScreen();
        }

        public void ButtonNewGameEventHandler(object sender, EventArgs e)
        {
            MainClass.action = ACTIONS.NEW_GAME;
            this.Close();
        }

        public void ButtonExitGameEventHandler(object sender, EventArgs e)
        {
            MainClass.action = ACTIONS.EXIT_ALL;
            this.Close();
        }

        public void CenterContainer()
        {
            if (root != null)
            {
                root.Location = new Point(this.Width / 2 - root.Width / 2,
              this.Height / 2 - root.Height / 2);
                root.Invalidate();
                root.Update();
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            CenterContainer();
        }
    }
}
