﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace SnakeZapoctovyProgramProgII
{
    public class DrawUtil
    {
        private readonly Form f;
        public DrawUtil(Form f) {
            this.f = f;
        }

        public void DrawRect(int x, int y, Color color, int size = 10)
        {
            Graphics g = this.f.CreateGraphics();
            SolidBrush brush = new SolidBrush(color);
            g.FillRectangle(brush, new Rectangle(x, y, size, size));
            brush.Dispose();
            g.Dispose();
        }
    }
}
