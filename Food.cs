﻿using System;
using System.Drawing;
using System.Windows.Forms;

using System.Collections.Generic;

namespace SnakeZapoctovyProgramProgII
{
    public class Food
    {
        public (int, int) position;
        public int size;
        public int scoreMovement;
        public Color color;

        private Food((int, int) position, int size, int scoreMovement, Color color)
        {
            this.position = position;
            this.size = size;
            this.scoreMovement = scoreMovement;
            this.color = color;
        }

        public static List<Food> GenerateFood(int count)
        {
            List<Food> f = new List<Food>();

            for (int i = 0; i < count; ++i)
            {
                Random r = new Random();
                int positionX = r.Next(0, 9) * 100;
                int positionY = r.Next(0, 9) * 100;
                (int, int) pos = (positionX, positionY);

                int curSize = r.Next(1, 3) * 10;

                int curScoreMovement = r.Next(1, 3);
                int curSign = r.Next(1, 3);

                if (curSign == 1) curScoreMovement *= -1;

                Color c;
                if (curScoreMovement < 0)
                {
                    c = Color.Red;
                }
                else
                {
                    c = Color.Gray;
                }

                f.Add(new Food(pos, curSize, curScoreMovement, c));
            }

            return f;
        }
    }
}
