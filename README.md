# Snake in CSharp

Jedná se o zápočtovou práci.

# Dokumentace

## DrawUtil.cs
- umí kreslit obdelníky (čtverce)

## Food.cs
- reprezentace jídla
- generování jídla různých velikostí a druhů
	- jídla dvou velikostí,
	- červená a šedá
		- červená jsou záporná, je třeba se jim vyhýbat
		- a šedá je kladná, ta inkrementuje skóre

## Game.cs
- samotná hra
- řeší rychlosti, pohyby, překreslování a další události
- časování řeší pomocí *Timer*u
	- počítá tiky o délce *10 ms*
	- každá událost se spouští po určitém množství tiků (ms)
		- přepočet řeší funkce *MsToTick()*
- konec hry nastane právě tehdy když se had kousne
	- pokud se had kousne, tak se přepneme do hlavního menu a zobrazíme skóre
- skóre může být záporné

## MainMenu.cs
- okno hlavního menu
- 2 možnosti
	- odejít ze hry (a vypnout program),
	- spustit novou hru, skóre se nastaví na 0.

## Program.cs
- řeší přepínání mezi okny

## Snake.cs
- reprezentace snaka
- řeší jeho prodlužování a pohyby (funkce *Next()*)
- pohyb řeším tak, že odstraním poslední část snaka a přidám další část snaka ve směru pohybu
- reprezentace snaka je LinkedList


# Diagram závislostí tříd

```mermaid
graph TD;
  Game -- zobrazuje hada --> Snake
  Game -- zobrazuje jídlo --> Food
  Game -- kreslí čtverečky --> DrawUtil
  Program -- spouští --> Game
  Program -- spouští --> MainMenu[Main menu]
  MainMenu -- iniciuje spuštění Game --> Program
  MainMenu -- ukončuje aplikaci --> id1((END))
```
