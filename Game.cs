﻿using System;
using System.Drawing;
using System.Windows.Forms;

using System.Collections.Generic;

namespace SnakeZapoctovyProgramProgII
{
    class Game : Form
    {
        // can draw rectangles (e.g. snake and food)
        private DrawUtil d;

        // tick counter
        private int tick = 0;
        // if FPS=10, then the tick counter is incremented once per 10ms
        private int fps = 10;

        /* Timing config
         * (in ms)
         */
        private int newSnakePartTime = 3000;
        private int refreshFoodTime = 6000;
        private int snakeSpeedTime = 200;
        private int snakeSpeedTimeMin = 100;
        private int snakeSpeedDecrement = 8;

        public static Timer myTimer;
        public static int score = 0;
        public static int foodCnt = 30;
        public static bool firstGame = true;
        public Snake s;
        public List<Food> f;

        public Game()
        {
            firstGame = false;
            myTimer = new Timer();
            this.d = new DrawUtil(this);
            this.InitComponent();
            score = 0;
            s = new Snake();
        }

        private void InitComponent()
        {
            // FRAME INIT - START
            this.Text = "Snake";
            this.BackColor = Color.FromArgb(90, 196, 00);
            this.ClientSize = new Size(550, 450);
            this.CenterToScreen();
            this.DoubleBuffered = true;
            KeyDown += KeyboardListener;
            // FRAME INIT - END

            // TIMING - START
            RegisterFrameUpdator(TickCounter);
            RegisterFrameUpdator(NewSnakePart);
            RegisterFrameUpdator(NewFood);
            RegisterFrameUpdator(RedrawBoard);

            // call each registered updator every 'fps'ms
            Game.myTimer.Interval = fps;

            // start timer
            Game.myTimer.Start();
            // TIMING - END

            // init food
            f = Food.GenerateFood(foodCnt);
        }


        /**
         * Takes delegate of type EventHandler.
         */
        public static void RegisterFrameUpdator(EventHandler e)
        {
            Game.myTimer.Tick += e;
        }

        /**
         * Increment tick counter
         */
        private void TickCounter(Object myObject, EventArgs myEventArgs)
        {
            tick += 1;
            if (tick % int.MaxValue == 0) tick = 0;
        }

        /**
         * Add new snake part each 
         */
        private void NewSnakePart(Object myObject, EventArgs myEventArgs)
        {
            if (tick % MsToTick(newSnakePartTime) == 0)
            {
                s.AddPart();
            }
        }

        /**
         * Redraw all components of the game board:
         *      - snake
         *      - food
         * Performs position checks.
         */
        private void RedrawBoard(Object myObject, EventArgs myEventArgs)
        {
            if (tick % MsToTick(snakeSpeedTime) == 0)
            {
                s.Next();
                Refresh();

                // draw snake
                foreach ((int, int) p in s.GetSnake())
                {
                    d.DrawRect(p.Item1, p.Item2, Color.Black);
                }

                // draw food
                foreach (Food food in f)
                {
                    d.DrawRect(food.position.Item1, food.position.Item2, food.color, food.size);
                }

                // check game state
                GameStateCheck();
            }
        }

        private void GameStateCheck()
        {
            // check snake collides with food
            foreach (Food food in f)
            {
                (int, int) foodPos = food.position;
                if (
                    (foodPos.Item1 <= s.GetHead().Item1 &&
                        s.GetHead().Item1 <= foodPos.Item1 + food.size) &&
                    (foodPos.Item2 <= s.GetHead().Item2 &&
                        s.GetHead().Item2 <= foodPos.Item2 + food.size)
                )
                {
                    /* it collides, so remove that food, increment snake speed,
                     * but do not go below 100ms
                     */                    
                    f.Remove(food);
                    if (snakeSpeedTime >= snakeSpeedTimeMin)
                        snakeSpeedTime -= snakeSpeedDecrement;
                    score += food.scoreMovement;
                    break;
                }
            }

            // check snake is eating itself
            if (s.IsSnakeEatingItself())
            {
                MainClass.action = ACTIONS.MAIN_MENU;
                myTimer.Stop();
                this.Close();
            }
        }

        /**
         * Generates new food when it is time for it.
         */
        private void NewFood(Object myObject, EventArgs myEventArgs)
        {
            if (tick % MsToTick(refreshFoodTime) == 0)
            {
                f = Food.GenerateFood(foodCnt);
            }
        }


        /**
         * Keyboard listener
         */
        private void KeyboardListener(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    s.ChangeHeadDirection(Direction.TOP);
                    break;
                case Keys.Down:
                    s.ChangeHeadDirection(Direction.BOTTOM);
                    break;
                case Keys.Right:
                    s.ChangeHeadDirection(Direction.RIGHT);
                    break;
                case Keys.Left:
                    s.ChangeHeadDirection(Direction.LEFT);
                    break;
            }
        }

        /**
         * Convert milliseconds to number of ticks.
         */
        public int MsToTick(int ms)
        {
            // 1 tick = FPS ms
            int requiredTickCount = ms / fps;
            return requiredTickCount;
        }
    }
}
