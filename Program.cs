﻿using System;
using System.Windows.Forms;

using System.Collections.Generic;

namespace SnakeZapoctovyProgramProgII
{
    public enum ACTIONS { EXIT_ALL, MAIN_MENU, NEW_GAME };

    class MainClass
    {
        public static ACTIONS action;

        public static void RunWindow<T>() where T : Form, new()
        {
            action = ACTIONS.EXIT_ALL;
            Application.Run(new T());
        }


        [STAThread]
        public static void Main(string[] args)
        {
            action = ACTIONS.MAIN_MENU;

            while (action != ACTIONS.EXIT_ALL)
            {
                switch (action)
                {
                    case ACTIONS.MAIN_MENU:
                        RunWindow<MainMenu>();
                        break;
                    case ACTIONS.NEW_GAME:
                        RunWindow<Game>();
                        break;
                }
            }
        }
    }
}
